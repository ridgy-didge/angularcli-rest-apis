import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { StarWarsApiComponent } from './star-wars-api/star-wars-api.component';
import { StarWarsApiService } from './star-wars-api/star-wars-api.service';
import { WpApiComponent } from './wp-api/wp-api.component';
import { WpApiService } from './wp-api/wp-api.service';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    StarWarsApiComponent,
    WpApiComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [StarWarsApiService, WpApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
