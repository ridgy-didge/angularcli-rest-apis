import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WpApiComponent } from './wp-api.component';

describe('WpApiComponent', () => {
  let component: WpApiComponent;
  let fixture: ComponentFixture<WpApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WpApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WpApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
