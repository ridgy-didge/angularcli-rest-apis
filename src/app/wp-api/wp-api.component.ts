import { Component, OnInit } from '@angular/core';
import { WpApiService } from './wp-api.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-wp-api',
  templateUrl: './wp-api.component.html',
  styleUrls: ['./wp-api.component.scss']
})
export class WpApiComponent implements OnInit {

  results: Object;
  postResults: Object;
  searchTerm$ = new Subject<string>();

  constructor( private userService: WpApiService ) {
  }
  postTitle = {};
  postContent = {};
 
  
  ngOnInit(): void {

    this.userService
      .getHome()
      .subscribe( data => {
        this.postTitle = data.title.rendered;
  			this.postContent = data.content.rendered;
    });

    this.userService
			.searchPosts(this.searchTerm$)
      .subscribe(results => {
      this.postResults = results;
      console.log(this.results);
      console.log(this.postResults);
    });
  }
}
