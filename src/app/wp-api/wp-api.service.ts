import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class WpApiService {


	private baseUrl: string = 'https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2';
	private queryPosts: string = '/posts/?search=';
	  
  	constructor ( private http: Http ) {}

	getHome() {
		return this.http
		.get(`${this.baseUrl}/posts/1/`)
		.map((res:Response) => res.json());
	}

	searchPosts(terms: Observable<string>) {
		return terms.debounceTime(400)
		.distinctUntilChanged()
		.switchMap(term => this.searchPostEntries(term));
	}

	searchPostEntries(term) {
	    return this.http
	    .get(this.baseUrl + this.queryPosts + term)
	    .map(res => res.json());
	}

}
