import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';

import { Person } from './person';


@Injectable()
export class StarWarsApiService {
	
	private baseUrl: string = 'https://swapi.co/api';
	private queryPlanet: string = '/planets/?search=';
	private queryPeople: string = '/people/?search=';
	private queryVehicle: string = '/vehicles/?search=';
	private querySpecies: string = '/species/?search=';
	private queryFilms: string = '/films/?search=';
	private querySpaceships: string = '/starships/?search=';
 

	constructor ( private http: Http  ) {}

	getAll() {
		return this.http
		.get(`${this.baseUrl}/`)
		.map((res:Response) => res.json());
	}

	getPlanets() {
		return this.http
		.get(`${this.baseUrl}/planets/`)
		.map((res:Response) => res.json());
	}

	getPeople(){
		return this.http
	  	.get(`${this.baseUrl}/people/`)
	  	.map((res:Response) => res.json());
	}

	getRandomPerson() {
		return this.http
	  	.get(`${this.baseUrl}/people/` + Math.floor((Math.random() * 87) +1))
	  	.map((res:Response) => res.json());
	}

	getRandomPlanet() {
		return this.http
	  	.get(`${this.baseUrl}/planets/` + Math.floor((Math.random() * 61) +1))
	  	.map((res:Response) => res.json());
	}
	

	searchPeople(terms: Observable<string>) {
    	return terms.debounceTime(400)
      	.distinctUntilChanged()
      	.switchMap(term => this.searchPeopleEntries(term));
	}

	searchPlanets(terms: Observable<string>) {
    	return terms.debounceTime(400)
      	.distinctUntilChanged()
      	.switchMap(term => this.searchPlanetEntries(term));
	}

	searchPeopleEntries(term) {
	    return this.http
	    .get(this.baseUrl + this.queryPeople + term)
	    .map(res => res.json());
	}

	searchPlanetEntries(term) {
	    return this.http
	    .get(this.baseUrl + this.queryPlanet + term)
	    .map(res => res.json());
	}

}



