import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarWarsApiComponent } from './star-wars-api.component';

describe('StarWarsApiComponent', () => {
  let component: StarWarsApiComponent;
  let fixture: ComponentFixture<StarWarsApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarWarsApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarWarsApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
