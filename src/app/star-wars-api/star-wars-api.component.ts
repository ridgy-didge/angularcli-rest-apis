import { Component, OnInit } from '@angular/core';
import { StarWarsApiService } from './star-wars-api.service';
import { Person } from './person';  // load the interface;
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-star-wars-api',
  templateUrl: './star-wars-api.component.html',
  styleUrls: ['./star-wars-api.component.scss'],
  providers: [StarWarsApiService]
})

export class StarWarsApiComponent {

  	searchTerm$ = new Subject<string>();
	planetTerm$ = new Subject<string>();
	results: Object;
	planetResults: Object;
	peopleResults: Object;
	allPlanets: Object;
	
	constructor( private userService: StarWarsApiService ) {
  	}

	initName = {};
	planets = {};
	initPlanet = {};
	initProfilesName = {};
	initProfilesHeight = {};
	initProfilesGender = {};
	initProfilesMass = {};
	initPlanetName = {};
	initPlanetPop = {};


	ngOnInit(): void {

		this.userService
			.getPlanets()
			.subscribe( data => {
	    	this.allPlanets = data.results;
	    });

   		this.userService
   			.getRandomPerson()
   			.subscribe( data => {
  			this.initProfilesName = data.name;
  			this.initProfilesHeight = data.height;
  			this.initProfilesGender = data.gender;
  			this.initProfilesMass = data.mass;
		});
		
    	this.userService
   			.getRandomPlanet()
   			.subscribe( data => {
  			this.initPlanetName = data.name;
  			this.initPlanetPop = data.population;
    	});

    	this.userService
			.searchPeople(this.searchTerm$)
      		.subscribe(results => {
        	this.peopleResults = results.results;
      	});

      	this.userService
			.searchPlanets(this.planetTerm$)
      		.subscribe(results => {
        	this.planetResults = results.results;
      	});
	}
}
