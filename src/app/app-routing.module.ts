import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { StarWarsApiComponent } from './star-wars-api/star-wars-api.component';
import { WpApiComponent } from './wp-api/wp-api.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },
    {
        path: 'star-wars-api',
        component: StarWarsApiComponent
    },
    {
        path: 'wp-api',
        component: WpApiComponent
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
